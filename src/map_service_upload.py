# Publishes a map service to an ArcGIS server
# A connection to ArcGIS Server must be established in the
#  Catalog window of ArcMap before running this script
import sys
import os

import arcpy
import xml.dom.minidom as DOM

def cleanup(sddraft=None, sd=None):
    try:
        if sddraft and os.path.exists(sddraft): os.remove(sddraft)
        if sd and os.path.exists(sd): os.remove(sd)
        print "Cleanup done"
    except:
        print "Cleanup failed"
        pass

def change_service_parameters(sddraft):
    # change service parameters
    doc = DOM.parse(sddraft)
    '''keys = doc.getElementsByTagName('Key')
    for key in keys:
        if key.firstChild.data == 'UsageTimeout': key.nextSibling.firstChild.data = 6000
        if key.firstChild.data == 'WaitTimeout': key.nextSibling.firstChild.data = 60
        if key.firstChild.data == 'IdleTimeout': key.nextSibling.firstChild.data = 20000
        if key.firstChild.data == 'MinInstances': key.nextSibling.firstChild.data = 1
        if key.firstChild.data == 'MaxInstances': key.nextSibling.firstChild.data = 1'''
    services___ = doc.getElementsByTagName('TypeName')
    for service__ in services___:
        if service__.firstChild.data == 'KmlServer':
            service__.parentNode.getElementsByTagName('Enabled')[0].firstChild.data = 'false'
            print "disable KmlServer"
        if service__.firstChild.data == 'WMSServer':
            service__.parentNode.getElementsByTagName('Enabled')[0].firstChild.data = 'true'
            print "enable WMSServer"

    # save changes
    f = open(sddraft,"w")
    doc.writexml(f)
    f.close()

def main():
    if len(sys.argv) != 2:
        print """Usage: {} input.tif """.format(sys.argv[0])
        sys.exit(-1)

    tifName = sys.argv[1]
    if tifName[-4:] != ".tif":
        print "invalid input file format, should use GeoTiff files"
        sys.exit(-1)

    name = tifName[:-4] # layer name
    wrkspc = 'C:/Users/yhuan125/arcpy/data'
    service = 'NEDproject_' + name + "_map"
    # copy from a empty mxd file
    mxd = arcpy.mapping.MapDocument(wrkspc + '/blank.mxd')
    mxdpath = wrkspc + '/' + service +'.mxd'
    mxd.saveACopy(mxdpath)
    print mxd.filePath
    del mxd

    # add a layer

    rasterPath30 = wrkspc + '/' + tifName
    rasterLayerName30 = name

    try:
        md = arcpy.mapping.MapDocument(mxdpath)
        df = arcpy.mapping.ListDataFrames(md)[0]
        result30 = arcpy.MakeRasterLayer_management(rasterPath30, rasterLayerName30)
        layer30 = result30.getOutput(0)
        arcpy.mapping.AddLayer(df, layer30, 'AUTO_ARRANGE')

        # save mxd
        md.save()
    except:
        print arcpy.GetMessages()+ "\n\n"
        sys.exit("Failed to map layer into mxd file")

    # Provide path to connection file
    # To create this file, right-click a folder in the Catalog window and
    #  click New > ArcGIS Server Connection
    con = 'C:/Users/yhuan125/AppData/Roaming/ESRI/Desktop10.3/ArcCatalog/arcgis on cab-cigilab-09.ad.uillinois.edu_6080 (publisher).ags'

    # Provide other service details
    sddraft = wrkspc + '/' + service + '.sddraft'
    sd = wrkspc + '/' + service + '.sd'
    summary = 'General reference map of the USA'
    tags = 'USA'

    # Create service definition draft
    try:
        arcpy.mapping.CreateMapSDDraft(md, sddraft, service, 'ARCGIS_SERVER', con, True, None, summary, tags)
    except:
        print arcpy.GetMessages()+ "\n\n"
        cleanup(Sddraft)
        sys.exit("Failed to create sddraft file")
        
    change_service_parameters(sddraft)

    # Analyze the service definition draft
    analysis = arcpy.mapping.AnalyzeForSD(sddraft)

    # Print errors, warnings, and messages returned from the analysis
    print "The following information was returned during analysis of the MXD:"
    for key in ('messages', 'warnings', 'errors'):
        print '----' + key.upper() + '---'
        vars = analysis[key]
        for ((message, code), layerlist) in vars.iteritems():
            print '    ', message, ' (CODE %i)' % code
            print '       applies to:',
            for layer in layerlist:
                print layer.name,
            print
    print ""

    # Stage and upload the service if the sddraft analysis did not contain errors
    if analysis['errors'] == {}:
        # Execute StageService. This creates the service definition.
        try:
            arcpy.StageService_server(sddraft, sd)
            # Execute UploadServiceDefinition. This uploads the service definition and publishes the service.
            url = arcpy.UploadServiceDefinition_server(sd, con)
            print "Service successfully published"
            print "url:", url
        except:
            print arcpy.GetMessages()+ "\n\n"
            cleanup(sddraft, sd)
            sys.exit("Failed to stage and upload service")
    else: 
        print "Service could not be published because errors were found during analysis."

    cleanup(sddraft, sd)
    
if __name__ == '__main__':
    main()
