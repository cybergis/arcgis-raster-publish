## ArcGIS raster publish

### Authors ###
Yiming Huang <yhuan125@illinois.edu>

### Summary ###
Receive a raster file as input, then use Arcpy to automatically publish the raster file onto an ArcGIS server.
The user could specify some configurations, including the server url, connection file location, type of service they want to get like WMS or KMS.

### Requirements ###
* ArcPy
ArcPy is a site package and you can only get it by installing ArcGIS for desktop.
Currently ArcGIS has only Windows verison desktop, so set up a Windows machine with ArcGIS desktop if you want to run the code

### Contents ###
* src/
    - map_service_load.py: publish a map service
    - image_service_load.py: publish an image service
* test/
